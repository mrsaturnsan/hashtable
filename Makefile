CC = clang++
CFLAGS = -std=c++1z -Og -g -Wall -Wextra -pedantic -Weffc++ -Wold-style-cast -Woverloaded-virtual -Wsign-promo  -Wctor-dtor-privacy -Wnon-virtual-dtor -Wreorder
FILE = main.cpp
OUT = output

build:
	@$(CC) $(CFLAGS) $(FILE) -o $(OUT)

run:
	@./$(OUT)

clean:
	@rm $(OUT)