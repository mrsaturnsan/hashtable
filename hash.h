/******************************************************************************/
/*
* @file   hash.h
* @author Aditya Harsh
* @brief  Rudimentary templated hash-table.
*/
/******************************************************************************/

#include <vector>     /* std::vector         */
#include <exception>  /* std::runtime_error  */
#include <utility>    /* std::pair           */

/**
 * @brief Aditya's templated library ;)
 * 
 */
namespace ATL
{
    /**
     * @brief Different types of probing systems.
     * 
     */
    enum class Probe
    {
        LINEAR,
        QUAD
    };

    /**
     * @brief Hash-table class. Uses Open Addressing for collision resolution.
     * 
     * @tparam VALUE 
     */
    template <typename KEY, typename VALUE>
    class Hash
    {
        // check the key type
        static_assert(std::is_fundamental<KEY>::value || std::is_same<KEY, std::string>::value, 
            "Please use either a fundamental type or std::string as your key.");

        public:

            /**
             * @brief Hash table constructor.
             * 
             * @param probe The probing algorithm used.
             */
            Hash(Probe probe = Probe::QUAD) noexcept : policy_(probe), count_(0)
            {
                map_.resize(InitialPrime());
            }

            /**
             * @brief Default destructor.
             * 
             */
            ~Hash() = default;

            /**
             * @brief Default copy.
             * 
             * @param other 
             */
            Hash(const Hash& other) = default;

            /**
             * @brief Default move.
             * 
             * @param other 
             */
            Hash(Hash&& other) = default;

            /**
             * @brief Default assignment.
             * 
             * @param other 
             * @return Hash<KEY, VALUE>& 
             */
            Hash<KEY, VALUE>& operator=(Hash&& other) = default;
            
            /**
             * @brief Inserts an element into the table.
             * 
             * @param id The id the data should be mapped to.
             * @param data The actual value of the data.
             */
            void Insert(KEY id, const VALUE& data)
            {
                // safety check
                if (count_ >= (map_.capacity() / 2))
                    Resize();

                // calculate the index to place
                unsigned index = IndexMap(id);

                // find an appropriate spot if colliding
                if (std::get<1>(map_[index]).state_ == State::OCCUPIED)
                {
                    if (std::get<0>(map_[index]) != id)
                    {
                        index = ResolveCollision(index);
                    }
                    else
                    {
                        // over-write data
                        map_[index] = {id, {State::OCCUPIED, data}};
                        return;
                    }
                }

                // write into the appropriate index
                map_[index] = {id, {State::OCCUPIED, data}};

                // increment
                ++count_;
            }

            /**
             * @brief Finds a value.
             * 
             * @param id The id of the element.
             * @return std::pair<bool, VALUE> The boolean indicates success or failure. The value is the actual element.
             */
            VALUE Find(KEY id) const
            {
                // ignore
                if (count_ == 0)
                    throw std::runtime_error("Empty hash table!");

                // calculate the index to search
                unsigned index = IndexMap(id);

                // search for the element
                index = SearchIndex(id, index);

                if (std::get<1>(map_[index]).state_ == State::OCCUPIED && std::get<0>(map_[index]) == id)
                    return std::get<1>(map_[index]).data_;
                else
                    throw std::runtime_error("Invalid index!");
            }

            /**
             * @brief Deletes an element.
             * 
             * @param id 
             */
            void Delete(KEY id) noexcept
            {
                // ignore
                if (count_ == 0) return;

                // calculate the index to search
                unsigned index = IndexMap(id);

                // search
                index = SearchIndex(id, index);

                if (std::get<1>(map_[index]).state_ == State::OCCUPIED && std::get<0>(map_[index]) == id)
                {
                    std::get<1>(map_[index]).state_ = State::DELETED;

                    --count_;

                    // clear out if empty
                    if (count_ == 0)
                        Clear();
                }
            }

            /**
             * @brief Clears the hash table.
             * 
             */
            void Clear() noexcept
            {
                // set all to empty
                for (auto& i : map_)
                    std::get<1>(i).state_ = State::EMPTY;

                // reset count
                count_ = 0;
            }

            /**
             * @brief Returns the total number of elements within the hash table.
             * 
             * @return unsigned 
             */
            unsigned Size() const noexcept
            {
                return count_;
            }

            /**
             * @brief Subscript overloard.
             * 
             */
            VALUE& operator[](KEY id) noexcept
            {
                // calculate the index to search
                unsigned index = IndexMap(id);

                // search for the element
                index = SearchIndex(id, index);

                if (std::get<1>(map_[index]).state_ == State::OCCUPIED && std::get<0>(map_[index]) == id)
                    return std::get<1>(map_[index]).data_;
                else
                {
                    // create a new index with junk
                    Insert(id, VALUE());
                    return this->operator[](id);
                }
            }

            /**
             * @brief Inserts into the hash table.
             * 
             * @param data 
             * @return Hash<KEY, VALUE>& 
             */
            Hash<KEY, VALUE>& operator+=(const std::pair<KEY, VALUE>& data)
            {
                Insert(data.first, data.second);
                return *this;
            }

        private:

            /**
             * @brief The different states for each index.
             * 
             */
            enum class State
            {
                EMPTY,
                OCCUPIED,
                DELETED
            };

            /**
             * @brief The data held by each index.
             * 
             */
            struct Data
            {
                Data(State state = State::EMPTY, const VALUE& data = VALUE()) noexcept : 
                    state_(state), data_(data){}
                State state_;
                VALUE data_;
            };

            /**
             * @brief Hash function.
             * 
             * @param id 
             * @return unsigned 
             */
            template <typename T>
            unsigned IndexMap(T id) const noexcept
            {
                if constexpr (std::is_same<T, std::string>::value)
                {
                    // pick upto the first 5 characters
                    unsigned sum = 0;

                    unsigned range = id.length() >= MaxStringFactor() ? MaxStringFactor() : id.length();

                    std::for_each(std::cbegin(id), std::cbegin(id) + range, 
                    [&sum](auto c)
                    {
                        sum += c - '0';
                    });

                    return sum % map_.capacity();
                }
                else
                {
                    return id % map_.capacity();
                }
            }

            /**
             * @brief Resolves hash table collisions.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned ResolveCollision(unsigned index) const
            {
                switch (policy_)
                {
                    case Probe::LINEAR:
                        return ResolveLinear(index);
                    case Probe::QUAD:
                        return ResolveQuad(index);
                    default:
                        throw std::runtime_error("Invalid policy");
                }
            }

            /**
             * @brief Linear-probe resolution.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned ResolveLinear(unsigned index) const noexcept
            {
                unsigned i = 1;
                unsigned cur_index = index;

                while (std::get<1>(map_[cur_index]).state_ == State::OCCUPIED)
                    cur_index = IndexMap(index + (++i));

                return cur_index;
            }

            /**
             * @brief Quadratic probe resolution.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned ResolveQuad(unsigned index) const noexcept
            {
                unsigned i = 1;
                unsigned cur_index = index;

                while (std::get<1>(map_[cur_index]).state_ == State::OCCUPIED)
                {
                    ++i;
                    cur_index = IndexMap(index + (i * i));
                }
                    
                return cur_index;
            }

            /**
             * @brief Finds the appropriate index.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned SearchIndex(KEY id, unsigned index) const
            {
                switch (policy_)
                {
                    case Probe::LINEAR:
                        return SearchLinear(id, index);
                    case Probe::QUAD:
                        return SearchQuad(id, index);
                    default:
                        throw std::runtime_error("Invalid policy");
                }
            }

            /**
             * @brief Linear-probe search.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned SearchLinear(KEY id, unsigned index) const noexcept
            {
                unsigned i = 1;
                unsigned cur_index = index;

                while (std::get<1>(map_[cur_index]).state_ != State::EMPTY && std::get<0>(map_[cur_index]) != id)
                    cur_index = IndexMap(index + (++i));
                    
                return cur_index;
            }

            /**
             * @brief Quadratic probe search.
             * 
             * @param index 
             * @return unsigned 
             */
            unsigned SearchQuad(KEY id, unsigned index) const noexcept
            {
                unsigned i = 1;
                unsigned cur_index = index;

                while (std::get<1>(map_[cur_index]).state_ != State::EMPTY && std::get<0>(map_[cur_index]) != id)
                {
                    ++i;
                    cur_index = IndexMap(index + (i * i));
                }

                return cur_index;
            }

            /**
             * @brief Resizes the hash table when it is full.
             * 
             */
            void Resize()
            {
                // store the old size
                unsigned old_size = map_.capacity();
                
                // expand
                map_.resize(NextPrime(map_.capacity() * 2));
                
                // reset data
                count_ = 0;

                // invalidate all positions, and collect indices of ones to shift
                for (unsigned i = 0; i < old_size; ++i)
                {
                    if (std::get<1>(map_[i]).state_ == State::OCCUPIED)
                    {
                        std::get<1>(map_[i]).state_ = State::DELETED;
                        Insert(std::get<0>(map_[i]), std::get<1>(map_[i]).data_);
                    }
                    else
                    {
                        std::get<1>(map_[i]).state_ = State::EMPTY;
                    }
                }
            }

            /**
             * @brief Checks for whether or not a number is prime.
             * 
             * @param num 
             * @return true 
             * @return false 
             */
            bool IsPrime(unsigned num) const noexcept
            {
                // check for one
                if (num == 1)
                    return false;

                // checks for factors
                for (unsigned i = 2; i <= (num / 2); ++i)
                {
                    if (num % i == 0)
                        return false;
                }

                return true;
            }

            /**
             * @brief Find the closest prime above number.
             * 
             * @param num 
             * @return unsigned 
             */
            unsigned NextPrime(unsigned num) const noexcept
            {
                // loop until prime found
                while (1)
                {
                    if (IsPrime(++num))
                        return num;
                }
            }

            /**
             * @brief Initial prime number to set the hash size to.
             * 
             * @return constexpr unsigned 
             */
            constexpr unsigned InitialPrime() const noexcept
            {
                return 59;
            }

            /**
             * @brief Higher values generally mean less collisions, but will require more time.
             * 
             * @return constexpr unsigned 
             */
            constexpr unsigned MaxStringFactor() const noexcept
            {
                return 5;
            }

            /**
             * @brief The map of data.
             * 
             */
            std::vector<std::pair<KEY, Data>> map_;

            /**
             * @brief The probing policy used by the hash table.
             * 
             */
            const Probe policy_;

            /**
             * @brief Total number of elemnts currently in the table.
             * 
             */
            unsigned count_;
    };
}
