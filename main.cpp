#include <iostream>
#include <random>

#include "hash.h"

#define PRINT(x) (std::cout << (x) << '\n')

int main()
{
    ATL::Hash<int, std::string> hash;

    hash[0] = "100";
    hash[3] = "200";

    try
    {
        hash.Find(-200);
    }
    catch(...)
    {
        PRINT("ERROR");
    }

    PRINT(hash[0]);
    
    return 0;
}
